$(function() {
    $('#btnSignUp').click(function() {
//        alert("Valid" + $("#form-signin").valid() ); >
        // alert("Valid" ); 
        // window.location.href = '/showSignin';
      
        $.ajax({
            url: '/signUp',
            data: $('form').serialize(),
            type: 'POST',
            success: function(response) {
                console.log(response);
                var wishObj = JSON.parse(response);
                if (wishObj.message=='User created successfully!') {
                    alert("Usuario creado exitosamente" ); 
                    window.location.href = '/showSignin';

                } else {
                    $('#alerts').text('Ocurrio un error, porfavor intente de nuevo.');
                }

            },
            error: function(error) {
                console.log(error);
            }
        });
    });
});
