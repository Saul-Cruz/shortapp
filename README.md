Shortapp

Web Service para acortar URLs

Aplicacion web que consiste de dos endpoints para acortar y desacortar urls respectivamente

Acortar URL:
curl -i -H "Content-Type: application/json" -X POST -d '{"url": "http://desarrolloweb.com"}' http://127.0.0.1:5000/api/short-url

Desacortar URL:
curl -i -H "Content-Type: application/json" -X GET 'http://127.0.0.1:5000/api/expand-url?url=http://bit.ly/2Ve44iy'