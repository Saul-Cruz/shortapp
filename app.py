1#!/usr/bin/python
from flask import Flask, jsonify, Response, g, request
from flask import render_template, json, session, redirect, flash
from werkzeug.security import generate_password_hash, check_password_hash
from werkzeug.wsgi import LimitedStream
import unicodedata
import MySQLdb,os
import json, socket, time, sys,datetime,re,requests
from datetime import date, timedelta
from requests.auth import HTTPBasicAuth
from io import StringIO
# from StringIO import StringIO

# ------------------------------------------------------------------------------------ INIT Configuration Values ------------------------------------------------------------------------------------ #

app = Flask(__name__)
app.config['JSONIFY_PRETTYPRINT_REGULAR']= False
app.secret_key = 'why would I tell you my secret key?'

@app.before_request
def db_connect():
  g.conn = MySQLdb.connect(host='127.0.0.1',
                           user='root',
                           passwd='elu',
                           db='shortapp', charset="utf8", use_unicode=True)
  g.cursor = g.conn.cursor()

@app.after_request
def db_disconnect(response):
  g.cursor.close()
  g.conn.close()
  return response

@app.route("/")
def main():
  return render_template('index.html')

@app.route('/logout')
def logout():
  session.pop('user',None)
  return redirect('/')
# ------------------------------------------------------------------------------------ END Configuration Values ------------------------------------------------------------------------------------ #

# ------------------------------------------------------------------------------------ INIT Login and sigin ------------------------------------------------------------------------------------ #
@app.route('/validateLogin',methods=['POST'])
def validateLogin():
  try:
    _username = request.form['inputEmail']
    _password = request.form['inputPassword']

    g.cursor.callproc('sp_validateLogin',(_username,))
    data = g.cursor.fetchall()
  
    if len(data) > 0:
      if check_password_hash(str(data[0][3]),_password):
        session['user']     = data[0][0]
        session['username'] = data[0][1]
        return redirect('/userHome')
      else:
        flash('Wrong Email address or Password.')
        return redirect('/showSignin')
    else:
      flash('Wrong Email address or Password.')
      return redirect('/showSignin')
 
 
  except Exception as e:
    flash(str(e))
    return redirect('/showSignin')
  finally:
    g.cursor.close()

@app.route('/showSignUp')
def showSignUp():
  return render_template('signup.html')


@app.route('/signUp',methods=['POST','GET'])
def signUp():
  try:
    _name = request.form['inputName']
    _email = request.form['inputEmail']
    _password = request.form['inputPassword']

    if _name and _email and _password:
        
      _hashed_password = generate_password_hash(_password)
      g.cursor.callproc('sp_createUser',(_name,_email,_hashed_password))
      data = g.cursor.fetchall()

      if len(data) is 0:
        g.conn.commit()
        return json.dumps({'message':'User created successfully!'})
      else:
        return json.dumps({'error':str(data[0])})
    else:
      return json.dumps({'html':'<span>Enter the required fields</span>'})

  except Exception as e:
    return json.dumps({'error':str(e)})
  finally:
    g.cursor.close() 
#        conn.close()

@app.route('/showSignin')
def showSignin():
  return render_template('signin.html')


# ------------------------------------------------------------------------------------ END Login and sigin ------------------------------------------------------------------------------------ #
# ------------------------------------------------------------------------------------ INIT User Home ------------------------------------------------------------------------------------ #
@app.route('/userHome')
def userHome():
  if session.get('user'):
    return render_template('userHome.html', session_user_name = session['username'])
  else:
    flash('Unauthorized Access')
    return redirect('/showSignin')

# ------------------------------------------------------------------------------------ END User Home ------------------------------------------------------------------------------------ #

#curl -i -H "Content-Type: application/json" -X POST -d '{"url": "http://desarrolloweb.com"}' http://127.0.0.1:5000/api/short-url
@app.route('/api/short-url', methods=['POST'])
def short_url():
  try:
    decoded = request.get_json()
    long_url=decoded['url']
    user_curl='saulcruz'
    user_key_curl='R_f8ce5113ec1d4bc2855393f11f77e7a8'
    url_post='http://api.bit.ly/shorten?version=2.0.1&longUrl='+str(long_url)+'&login='+str(user_curl)+'&apiKey='+str(user_key_curl)
    # data='{"version":2.0.1,"longUrl":"'+str(long_url)+'","login":"'+str(user_curl)+'","apiKey":"'+str(user_key_curl)+'"}'
    r=requests.post(url_post)
    result=r.text
    read_result=json.loads(result)
    print(result)
    msg=''
    if read_result['statusCode']=='OK':
      msg=read_result['results'][long_url]['shortUrl']
      resp = Response(json.dumps({'error': 0, 'result':msg}), status=201, mimetype='application/json')
    else:
      msg=read_result['errorMessage']
      resp = Response(json.dumps({'error': 1, 'errorMsg':msg}), status=200, mimetype='application/json')
    return resp
  except Exception as e:
    resp = Response(json.dumps({'error': 1, 'errorMsg':str(e)}), status=200, mimetype='application/json')
    return resp

#curl -i -H "Content-Type: application/json" -X GET 'http://127.0.0.1:5000/api/expand-url?url=http://bit.ly/2Ve44iy'  
@app.route('/api/expand-url', methods=['GET'])
def expand_url():
  try:
    short_url=request.args.get('url')
    user_curl='saulcruz'
    user_key_curl='R_f8ce5113ec1d4bc2855393f11f77e7a8'
    url_get='http://api.bit.ly/expand?version=2.0.1&shortUrl='+str(short_url)+'&login='+str(user_curl)+'&apiKey='+str(user_key_curl)
    r=requests.get(url_get)
    result=r.text
    read_result=json.loads(result)
    print(result)
    msg=''
    if read_result['statusCode']=='OK':
      msg=read_result['results'][short_url.replace('http://bit.ly/','')]['longUrl']
      resp = Response(json.dumps({'error': 0, 'result':msg}), status=201, mimetype='application/json')
    else:
      msg=read_result['errorMessage']
      resp = Response(json.dumps({'error': 1, 'errorMsg':msg}), status=200, mimetype='application/json')
    return resp
  except Exception as e:
    resp = Response(json.dumps({'error': 1, 'errorMsg':str(e)}), status=200, mimetype='application/json')
    return resp

if __name__ == '__main__':
    app.run(debug=True)

if __name__ == '__main__':
    app.run(host='0.0.0.0',port=3000)